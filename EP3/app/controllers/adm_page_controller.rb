class AdmPageController < ApplicationController
 before_action :authenticate_admin!

  def index
  	@users = User.all
  	@admins = Admin.all
  	@book = Book.all
    @borrow = Borrow.all
    @album = Album.all
    @game = Game.all
  end

   def destroy
    @user = User.find(params[:id])
    @user.destroy
  end
  
end
