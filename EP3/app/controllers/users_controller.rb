class UsersController < ApplicationController
before_action :authenticate_admin!

	def destroy
    @book = Book.all
    @borrow = Borrow.all
    @album = Album.all
    @game = Game.all
    @user = User.find(params[:id])
    puts @user

    validation = @user.id.to_s

    @book.each do |book|
    	if(book.iduser == validation)
    		book.destroy
    	end
    end

    @borrow.each do |borrow|
    	if(borrow.iduser == validation)
    		borrow.destroy
    	end
    end

    @album.each do |album|
    	if(album.iduser == validation)
    		album.destroy
    	end
    end

    @game.each do |game|
    	if(game.iduser == validation)
    		game.destroy
    	end
    end        

    email = @user.email
    @user.destroy

    	if @user.destroy
       		redirect_to adm_page_index_path, notice: "O usuario #{email} e todos os seus cadastros foram deletados do sistema com sucesso!"
    	end
    end	
end
