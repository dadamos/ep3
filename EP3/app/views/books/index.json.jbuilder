json.array!(@books) do |book|
  json.extract! book, :id, :iduser, :title, :author, :description, :borrow
  json.url book_url(book, format: :json)
end
