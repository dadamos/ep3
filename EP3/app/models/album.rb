class Album < ActiveRecord::Base
	validates_presence_of :band, :description
	validates :name, presence: true
end
