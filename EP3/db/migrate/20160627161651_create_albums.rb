class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :iduser
      t.string :band
      t.string :description
      t.boolean :borrow

      t.timestamps null: false
    end
  end
end
