class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :iduser
      t.string :title
      t.string :developer
      t.string :description
      t.boolean :borrow

      t.timestamps null: false
    end
  end
end
